-- Potrebno je rangirati zaposlenike prema broju različitih filmova koje su iznajmili. 
-- Pored ranga potrebno je ispisati identifikator, prezime i ime zaposlenika te 
-- broj filmova (različitih) koji se "pripisuju" tom zaposleniku. 
-- Zaposleniku se "pripisuju" primjerci filmova je iznajmio on ili 
-- njegovi, direktno ili indirektno, podređeni zaposlenici. 
-- Uzeti u obzir i primjerke koji nisu vraćeni.


SELECT	rank() 	OVER (ORDER BY cnt DESC) as rank,
		rented.*
FROM (
		SELECT	staff_main.staff_id,
		        staff_main.last_name,
				staff_main.first_name,
				(
					WITH RECURSIVE subordinates AS 
       			     (
   						SELECT	staff_id,
      							sup_staff_id,
      							first_name,
      							last_name
 		  				FROM staff
   						WHERE staff.staff_id = staff_main.staff_id 
  						UNION
      					SELECT	sub.staff_id,
      							sub.sup_staff_id,
      							sub.first_name,
      							sub.last_name
		      			FROM staff AS sub
     		 			INNER JOIN subordinates s 
      		          ON s.staff_id = sub.sup_staff_id	
					) 
					SELECT COUNT (DISTINCT inventory.film_id)
					FROM subordinates
					LEFT JOIN rental
					ON subordinates.staff_id = rental.staff_id
                    LEFT JOIN inventory
                    ON rental.inventory_id = inventory.inventory_id
				) AS cnt
		FROM staff AS staff_main
		ORDER BY staff_main.staff_id
) AS rented
