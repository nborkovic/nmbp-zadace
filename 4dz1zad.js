// Korištenjem kolekcije 'dvdrent' i M/R upita potrebno je za svakog zaposlenika iz države Philippines pronaći njegove klijente. Kao rezultat potrebno je izračunati i prikazati broj njegovih klijenata 
// koji su također podrijetlom iz države **Philippines te uz njih i broj klijenata koji su iz ostalih država.
// Pod brojem klijenata mislis se na broj različitih korisnika usluge.
// Nazivi atributa i format se vide iz primjera rezultata:
/*
  {
    "_id": {
      "name": "Alice",
      "lastname": "Griffin"
    },
    "value": {
      "phil": 6,
      "other": 132
    }
  },
  {
    "_id": {
      "name": "Beverly",
      "lastname": "Lewis"
    },
    "value": {
      "phil": 6,
      "other": 120
    }
  },
*/



db.dvdrent.mapReduce (
	`function() {
		if(this.staff.address.country == 'Philippines'){
			var key =  { name: this.staff.first_name, lastname: this.staff.last_name}
			if (this.customer.address.country == 'Philippines') {
				emit (key, {key: key, customer_id: "??" +this.customer.customer_id+"??", phil: 1, other: 0}); 
			} else {
				emit (key, {key: key, customer_id: "??" +this.customer.customer_id+"??", phil: 0, other: 1}); 
			}   
		} 
	};
   	`,
	`function(key, values) {
		var result = { key: key, customer_id : "_", phil: 0, other: 0 };

		values.forEach( function (value) {
			if (! result.customer_id.includes(value.customer_id)){
				result.customer_id += value.customer_id;
				result.phil += value.phil;
				result.other += value.other;
			}
		});
	        return result;  
        };
	`,
        { 
	   out: 'mr_out',
           finalize: 
           `	function (key, result) {
			return {phil: result.phil, other: result.other}
		}
           `
        }
);



