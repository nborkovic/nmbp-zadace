-- Pretpostavite da želite naći najbližu zalihu vode vatrogasnim domovima.
-- Za svaku zgradu koja u nazivu sadrži niz znakova "vatrog" ili "dvd" (bez obzira na velika ili mala slova) 
-- ispisati ime zgrade i udaljenost do najbližeg vodenog objekta ili rijeke (water, "water", "river") 
-- i naziv tog vodenog objekta (mora biti definiran).
-- Udaljenost zaokružite na cijeli broj.


SELECT distances.building_name, distances.distance, distances.water_name
FROM (	SELECT	
    			buildings.name AS building_name,
    			buildings.geom AS building_geometry,
				MIN(round(st_distance(buildings.geom, water.geom))) OVER (PARTITION BY buildings.name) distance,
        		water.name AS water_name,
        		water.geom AS water_geometry
		FROM buildings, water
		WHERE (lower(buildings.name) LIKE '%dvd%' OR lower(buildings.name) LIKE '%vatrog%')
		AND (water.fclass = 'water' OR water.fclass = 'river') 
		AND water.name IS NOT NULL
		ORDER BY building_name
) AS distances
WHERE round(st_distance(distances.building_geometry, distances.water_geometry)) = distances.distance
