-- Ispisati naslov filma, godinu (datuma rental.rental_date) i broj iznajmljivanja primjeraka filma u toj godini (godinu odredite prema atributu rental.rental_date). Dodatno, ispisati kumulativni broj
-- iznajmljivanja (cntThisAndPrevYear), koji obuhvaća i iznajmljivanja iz svih prethodnih godina. U obzir uzeti samo filmove koji u naslovu sadrže riječ koja će uz najviše 2 promjene biti jednaka riječi
-- 'flintstons'.

﻿CREATE TEMP TABLE filmCnt AS 
SELECT film.title,
	levenshtein (lower(film.title), lower('flintstons') ) AS steps,
	EXTRACT (YEAR FROM rental.rental_date)::INT as year,
	COUNT (*) AS cntYear
FROM film 
LEFT JOIN inventory ON film.film_id = inventory.film_id
LEFT JOIN rental ON rental.inventory_id = inventory.inventory_id
WHERE (
	SELECT COUNT(*) AS numOfMatchigngWords
	FROM ts_debug ('english', film.title)
	WHERE levenshtein (lower(token), lower('flintstons') )<3
) >= 1
GROUP BY film.title, year
ORDER BY film.title;

SELECT filmCnt.*,
       SUM (cntyear) OVER (PARTITION BY filmCnt.title
			   ORDER BY filmCnt.year
        ) AS cntThisAndPrevYears
FROM filmCnt;
