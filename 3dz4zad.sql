-- Pretpostavite da želite za sve šume i vatrogasne postaje pronaći njihovu međusobnu udaljenost 
-- i udaljenost pojedine vatrogasne postaje do najbližih zaliha vode.
-- Za svaku šumu (landuse, "forest") kojoj je definirano ime i zgradu koja u nazivu sadrži niz znakova 
-- "vatrog" ili "dvd" (bez obzira na velika ili mala slova) ispisati:
-- ime šume, udaljenost do zgrade, ime zgrade, udaljenost zgrade do najbližeg vodenog objekta 
-- ili rijeke (water, 'water' ili 'river') te ime tog vodenog objekta (mora biti definirano).
-- Zapise poredati uzlazno prema imenu šume, a zatim prema udaljenosti.
-- Udaljenosti zaokružite na cijeli broj.

SELECT 	landuse.name AS forest_name,
		(SELECT round(st_distance(landuse.geom, nearest_water.building_geometry))) AS distance_between_forest_and_fire_dept,
        nearest_water.building_name,
        nearest_water.distance AS distance_between_fire_dept_and_nearest_water,
        nearest_water.water_name
FROM(
	SELECT 	distances.building_name AS building_name, 
    		distances.building_geometry AS building_geometry, 
    		distances.distance AS distance, 
    		distances.water_name AS water_name
	FROM (	SELECT	
    				buildings.name AS building_name,
    				buildings.geom AS building_geometry,
					MIN(round(st_distance(buildings.geom, water.geom))) OVER (PARTITION BY buildings.name) distance,
        			water.name AS water_name,
        			water.geom AS water_geometry
			FROM buildings, water
			WHERE (lower(buildings.name) LIKE '%dvd%' OR lower(buildings.name) LIKE '%vatrog%')
			AND (water.fclass = 'water' OR water.fclass = 'river') 
			AND water.name IS NOT NULL
			ORDER BY building_name
	) AS distances
	WHERE round(st_distance(distances.building_geometry, distances.water_geometry)) = distances.distance
)AS nearest_water, landuse
WHERE landuse.fclass = 'forest'
AND landuse.name IS NOT NULL
ORDER BY forest_name, distance_between_forest_and_fire_dept

-- NOTE: Ovaj zadatak je nadogradnja na prvi zadatak
