// Koristeći kolekciju dvdrent, za sve filmove dulje od 120 minuta koji imaju rejting "G" izračunajte koliko puta su posuđeni, koliko su ukupno uprihodili, koliko dana je proteklo od prvog do zadnjeg
// iznajmljivanja filma i koliko je film uprihodio po danu (ukupan prihod podijeljen s brojem dana).
// U obzir uzeti samo zapise koji imaju evidentirano plaćanje (dvdrent). Iznos zaokružiti na dvije decimale, a iznos po danu na tri.

// Nazivi atributa i format se vide iz primjera rezultata:

/*
 [ 
 {
    "_id": {
      "title": "Academy Dinosaur"
    },
    "value": {
      "rentals": 21,
      "amount": 33.79,
      "days": 2883,
      "amountPerDay": 0.012
    }
  },
  (...)
]
*/

db.dvdrent.mapReduce (
	`function() {
		if(this.film.length > 120 && this.payment != null && this.film.rating == "G"){
			var key =  { title: this.film.title};
			var amount = this.payment.amount;
			var dates = [];

			dates.push(new Date(this.rental_date))
			emit (key, {key: key, rentals: 1, amount: amount, dates: dates }) 
		} 
	};
   	`,
	`function(key, values) {
		var result = { key: key, rentals: 0, amount: 0, dates: [] }
		values.forEach( function (value) {
			result.rentals += value.rentals;
			result.amount += value.amount;
			value.dates.forEach ( function (date) {
				result.dates.push(date);
			})
		});
	        return result;  
        };
	`,
        { 
	   out: 'mr_out',
           finalize: 
           `	function (key, result) {

			var amount = Math.round(result.amount * 100) / 100
			var rentals = Math.round(result.rentals * 100) / 100
			var maxDate = new Date(Math.max.apply(null,result.dates));
			var minDate = new Date(Math.min.apply(null,result.dates));
			var diffDays = Math.round((maxDate-minDate)/(1000*60*60*24));
			var amountPerDay = result.amount / diffDays;
			amountPerDay = Math.round(amountPerDay * 1000) / 1000

			return { rentals: rentals, amount: amount, days: diffDays, amountPerDay: amountPerDay}
		}
           `
        }
);
























