-- Pronaći općine (muny) u Splitsko-dalmatinskoj županiji (muny, name_1) kroz koje prolazi pruga.
-- Potrebno je ispisati naziv općine (muny, name_2) i duljinu pruge (railways) koja prolazi kroz tu općinu.
-- Zapise poredati uzlazno s obzirom na duljinu pruge koja prolazi općinom.

SELECT muny.name_2 AS  municipality,
	   SUM (st_length(st_intersection(railways.geom, muny.geom))) AS railway_length
FROM muny, railways
WHERE muny.name_1 = 'Splitsko-Dalmatinska' 
AND st_intersects(muny.geom, railways.geom)
GROUP BY municipality
ORDER BY railway_length
