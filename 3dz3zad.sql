-- Pretpostavite da želite za sve vatrogasne postaje (buildings) naći 2 najbliže rijeke (water, "river") i udaljenost do njih.
-- Za svaku zgradu koja u nazivu sadrži niz znakova "vatrog" ili "dvd" (bez obzira na velika ili mala slova) 
-- ispisati: ime zgrade, ime rijeke, udaljenost do rijeke i redni broj.
-- Rezultate poredajte po imenu zgrade i rednom broju rijeke.
-- Udaljenosti zaokružite na cijeli broj. 


SELECT * 
FROM (
		SELECT all_distances_between_fire_depts_and_rivers.*, 
			   rank() OVER (PARTITION BY all_distances_between_fire_depts_and_rivers.dep_name
                    		ORDER BY all_distances_between_fire_depts_and_rivers.distance
               			   ) AS rank
		FROM(
    			SELECT	buildings.name AS dep_name,
	   					water.name AS river_name,
						round(st_distance(water.geom, buildings.geom)) AS distance
				FROM  buildings, water
				WHERE (lower(buildings.name) LIKE '%dvd%' OR lower(buildings.name) LIKE '%vatrog%')
				AND water.fclass = 'river' 
				ORDER BY dep_name
		)AS all_distances_between_fire_depts_and_rivers
	) AS ranked_distances
WHERE rank <=2
