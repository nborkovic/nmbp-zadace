-- Ispisati naslov filma i ukupne iznose zarađene iznajmljivanjem primjeraka filma po kvartalima godine.
-- U obzir uzeti samo filmove koji bilo u nazivu bilo u opisu sadrže riječ čiji je normalizirani oblik 'fanci' ili 'amus'. Možete smatrati da je iznos zarađen na dan kada je plaćen. Napomena: Kod spajanja
-- s tablicom payment zanemarite ulogu atributa payment.staff_id.
-- NAPOMENA2: Kod računanja zarade, napravite CAST u tip numeric(10,2); Za atribut film.title u pivot tablici zadržite originalni tip podatka. 

CREATE TEMP TABLE quartals (quartal INT);

INSERT INTO quartals VALUES (1); 
INSERT INTO quartals VALUES (2); 
INSERT INTO quartals VALUES (3); 
INSERT INTO quartals VALUES (4); 

SELECT * FROM crosstab (
'
SELECT film.title,

(SELECT 
	CASE 
		WHEN (EXTRACT (MONTH FROM payment_date)::INTEGER ) BETWEEN 1 AND 3 THEN 1
		WHEN (EXTRACT (MONTH FROM payment_date)::INTEGER ) BETWEEN 4 AND 6 THEN 2
		WHEN (EXTRACT (MONTH FROM payment_date)::INTEGER ) BETWEEN 7 AND 9 THEN 3
		WHEN (EXTRACT (MONTH FROM payment_date)::INTEGER ) BETWEEN 10 AND 12 THEN 4
	END
)AS quartal,
CAST (SUM (payment.amount) AS NUMERIC(10,2)) AS sumOfamounts
FROM film 
LEFT JOIN inventory on film.film_id = inventory.film_id
LEFT JOIN rental on rental.inventory_id = inventory.inventory_id 
RIGHT JOIN payment on payment.rental_id = rental.rental_id
WHERE to_tsvector(''english'', title) @@ to_tsquery(''english'', ''fanci | amus'')
OR  to_tsvector(''english'', description) @@ to_tsquery(''english'', ''fanci | amus'')
GROUP BY film.title, quartal
ORDER BY film.title, quartal
',
'SELECT quartal FROM quartals'
)
AS pivotTable (filmTitle VARCHAR(255), q1 NUMERIC(10,2), q2 NUMERIC(10,2), q3 NUMERIC(10,2), q4 NUMERIC(10,2));
