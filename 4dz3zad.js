// Pomoću kolekcije dvdRent, doznajte ukupan zarađeni iznos i broj naplaćenih posudbi koje su pojedini zaposlenici naplatili po pojedinim kategorijama (payment mora postojati). 
// Ispisati samo zaposlenike koji žive u zemlji United Kingdom. U slučaju da film pripada više kategorija (npr. "love" i "comedy"), i zarada i posudba se pripisuju i kategoriji
//  "love" i kategoriji "comedy".

// Ukupan iznos zaokružiti na dvije decimale.

// Npr. Zaposlenik Adam Ward je za komedije naplatio ukupnan iznos 85.72 dolara za 15 primjeraka filmova, a za filmove katastrofe 32.93 dolara za 8 primjeraka.

// Primjer rezultata (stvarni podaci se mogu razlikovati):

/*
[
  {
    "_id": {
      "name": "Adam",
      "lastname": "Ward",
      "category": "Comedy"
    },
    "value": {
      "amount": 85.72,
      "count": 15
    }
  },
  {
    "_id": {
      "name": "Adam",
      "lastname": "Ward",
      "category": "Catastrophy"
    },
    "value": {
      "amount": 32.93,
      "count": 8
    }
  },
  ...
]
*/


db.dvdrent.mapReduce (
	`function() {
		if(this.staff.address.country == 'United Kingdom' && this.payment != null){
			var name = this.staff.first_name;
			var lastname = this.staff.last_name;
			var amount = this.payment.amount;
			this.film.categories.forEach( function (category) {
				var key = {name: name, lastname: lastname, category: category.name}
				var value = {key: key, amount: amount , count: 1}
				emit(key, value);
			} )
		} 
	};
   	`,
	`function(key, values) {
		var result = { key: key, amount: 0, count: 0 }
		values.forEach( function (value) {
			result.amount += value.amount;
			result.count += value.count;
		});
	        return result;  
        };
	`,
        { 
	   out: 'mr_out',
           finalize: 
           `	function (key, result) {

			var amount = Math.round(result.amount * 100) / 100
			return { amount: amount, count: result.count }
		}
           `
        }
);


