-- Za plaže (nature, "beach") koje u krugu od 300 metara imaju barem 3 kafića ili restorana 
-- (pois, "bar" i "restaurant") ispišite gid, ime plaže i ukupan broj kafića/restorana u krugu od 300 metara.

SELECT	nature.gid AS nature_gid,
		nature.name AS nature_name,
        COUNT(*) AS number_of_objects
FROM nature, pois
WHERE nature.fclass = 'beach'
AND (pois.fclass = 'bar' OR  pois.fclass = 'restaurant')
AND st_intersects(st_buffer(nature.geom, 300, 'quad_segs=8'), pois.geom)
GROUP BY nature_gid, nature_name
HAVING COUNT(*) >= 3 
