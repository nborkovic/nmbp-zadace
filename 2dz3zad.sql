-- Ispisati identifikator, prezime i ime zaposlenika te broj posudbi filmova koje se "pripisuju" tom zaposleniku. 
-- Zaposleniku se "pripisuju" sve posudbe koje je odradio on sam ili njemu podređeni zaposlenici (direktno i indirektno podređeni).


SELECT	staff_main.staff_id,
        staff_main.last_name,
		staff_main.first_name,
		(
			WITH RECURSIVE subordinates AS 
            (
   				SELECT	staff_id,
      					sup_staff_id,
      					first_name,
      					last_name
   				FROM staff
   				WHERE staff.staff_id = staff_main.staff_id 
  				UNION
      			SELECT	sub.staff_id,
      					sub.sup_staff_id,
      					sub.first_name,
      					sub.last_name
      			FROM staff AS sub
      			INNER JOIN subordinates s 
                ON s.staff_id = sub.sup_staff_id
			) 
			SELECT COUNT(*)
			FROM subordinates
			LEFT JOIN rental
			ON subordinates.staff_id = rental.staff_id
		) AS cnt
FROM staff AS staff_main
ORDER BY staff_main.staff_id 
