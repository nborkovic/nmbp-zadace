-- Za kategorije u kojima postoje filmovi koji u nazivu sadrže riječ nalik riječi 'anakonda' 
-- ispisati naziv kategorije te jezike na kojima su dostupni filmovi u toj kategoriji. 
-- Jezici moraju biti uzlazno poredani prikazani u obliku u polja (array).
-- Nalikuje li riječ iz naslova filma riječi 'anakonda' ispitajte pomoću 
-- implementacije Q-gram algoritma u PostgreSQL-u.
-- Zapise tablice sortirati uzlazno prema nazivu kategorije.

SELECT distinct	
		category_main.name, 
		(SELECT ARRAY_AGG (lang) 
         FROM                
                (
                SELECT DISTINCT			
				category.name AS cat,
				language.name AS lang
	           	FROM category
				LEFT JOIN film_category
				ON  category.category_id = film_category.category_id
				LEFT JOIN film
				ON film_category.film_id = film.film_id
				LEFT JOIN language 
				ON language.language_id = film.language_id	
                WHERE category.category_id = category_main.category_id
				ORDER BY category.name, language.name
                ) AS languages
         )
FROM category AS category_main
LEFT JOIN film_category
ON  category_main.category_id = film_category.category_id
LEFT JOIN film
ON film_category.film_id = film.film_id
WHERE similarity (film.title, 'anakonda') > 0.2
